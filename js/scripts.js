$(window).on("load", function() {
    var byMenuBtn = true;
    var byMailBtn = false;
    var closeMailForm = true;
    // Click on menu icon
    $('.menu-ico').on('click', function() {
        // scale circle
        if (!byMailBtn) {
            var $circle = $('.circle-menu');
            if (!byMenuBtn) {
                $(this).removeClass('close-nav');
                $('.circle-menu').removeClass('opened');
                $('.logo-block-main').removeClass('menu-opened');
                setTimeout(function() {
                    $('.main-nav-block').removeClass('menu-opened');
                    $('body').removeClass('locked');
                }, 150)
                TweenMax.to($circle, 0.5, {
                    scale: 1
                });
                byMenuBtn = true;
            } else {
                $(this).addClass('close-nav');
                $('.circle-menu').addClass('opened');
                $('.logo-block-main').addClass('menu-opened');
                setTimeout(function() {
                    $('.main-nav-block').addClass('menu-opened');
                }, 150)
                $('body').addClass('locked');
                TweenMax.to($circle, 2, {
                    scale: 500
                });
                byMenuBtn = false;
            }
        } else {
            closeMailForm = true;
            byMailBtn = false;
            var $mailActivator = $('.circle-form');
            TweenMax.to($mailActivator, 0.5, {
                scale: 1
            });
            $('.mail-ico').removeClass('close-nav');
            $(this).removeClass('close-nav');
            $('.circle-menu').removeClass('opened');
            setTimeout(function() {
                $('.circle-form').removeClass('opened');
            }, 400);
			setTimeout(function() {
                $('.mail-popup-block').removeClass('menu-opened');
            }, 300);
            $('body').removeClass('locked');
        }
    }); // Click on menu icon
    $('.mail-ico').on('click', function() {
        if (closeMailForm) {
            byMailBtn = true;
            var $mailActivator = $('.circle-form');
            $(this).addClass('close-nav');
            $('.menu-ico').addClass('close-nav');
            $('.circle-menu').addClass('opened');
            $('.circle-form').addClass('opened');
            setTimeout(function() {
                    $('.mail-popup-block').addClass('menu-opened');
                }, 150)
            $('body').addClass('locked');
            TweenMax.to($mailActivator, 2, {
                scale: 500
            });
        } else {
            closeMailForm = false;
        }
    })
// mail-popup-block
$('.mail-ico').hover(function() {
        $('#dark-mail-icon').addClass('visible');
        $('#light-mail-icon').removeClass('visible');
},
function() {
    if (!$(window).scrollTop()) {
        $('#dark-mail-icon').removeClass('visible');
        $('#light-mail-icon').addClass('visible');
    }
});
    //Scroll events
}); //$(window).on("load", function()...
$(window).scroll(function() {
    if ($(window).scrollTop()) {
        $('.mail-ico').addClass('onScroll');
        $('.menu-ico').addClass('onScroll');
        $('#dark-mail-icon').addClass('visible');
        $('#light-mail-icon').removeClass('visible');
        //$('.mail-activator-b').addClass('visible');
        $('.mail-activator-w').addClass('closed');
    } else {
        $('.mail-ico').removeClass('onScroll');
        $('.menu-ico').removeClass('onScroll');
        $('#dark-mail-icon').removeClass('visible');
        $('#light-mail-icon').addClass('visible');
        //$('.mail-activator-b').removeClass('visible');
        $('.mail-activator-w').removeClass('closed');
    }
}); //Scroll events[END]